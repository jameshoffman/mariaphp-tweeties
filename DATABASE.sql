DROP TABLE IF EXISTS likes, tweeties, users;

-- 
-- USERS
-- 

CREATE TABLE users (
  	id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	username VARCHAR(50) NOT NULL UNIQUE,
    pwd VARCHAR(50) NOT NULL
);

INSERT INTO users(username, pwd) 
VALUES ('alice', 'pwda'), ('bob', 'pwdb'), ('charlie', 'pwdc');


--
-- TWEETIES
--

CREATE TABLE tweeties(
	id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
 	text VARCHAR(240) NOT NULL,
  	created_at DATETIME DEFAULT NOW(),
  	user_id INT UNSIGNED NOT NULL,

  	FOREIGN KEY (user_id) REFERENCES users(id)
	ON DELETE CASCADE
);

INSERT INTO tweeties (text, created_at, user_id)
VALUES  ('My first tweetie', DATE_ADD(NOW(), INTERVAL -36 HOUR), 1),
        ("Another tweetie", DEFAULT, 1),
        ("Tweetie, here I am!!!1!11!!", DATE_ADD(NOW(), INTERVAL -49 HOUR), 2);

--
-- LIKES
--

CREATE TABLE likes (
	user_id INT UNSIGNED NOT NULL,
	tweety_id INT UNSIGNED NOT NULL,

	PRIMARY KEY (user_id, tweety_id),

	FOREIGN KEY (user_id) REFERENCES users(id)
	ON DELETE CASCADE,

	FOREIGN KEY (tweety_id) REFERENCES tweeties(id)
	ON DELETE CASCADE
);

INSERT INTO likes
VALUES (1, 1), (2, 1), (2, 2);

--
-- Hashed password
--

ALTER TABLE users
MODIFY COLUMN pwd VARCHAR(64) NOT NULL;

UPDATE users
SET pwd = SHA2(pwd, 256);