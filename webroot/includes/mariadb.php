<?php

$mysqli = new mysqli(get_config('DB_HOST'), 
                    get_config('DB_USER'), 
                    get_config('DB_PASSWORD'), 
                    get_config('DB_NAME'), 
                    get_config('DB_PORT'));

if ($mysqli->connect_errno) {
    set_flash('error', '<h1 style="color: red;">CONFIG</h1>Could not connect to database : ' . $mysqli->connect_error);
}

function sql($query, $params = []) {
    if (get_config('DEBUG')) {
        error_log("---");
        error_log($query);
        error_log(json_encode($params));
        error_log("---");
    }
    global $mysqli;

    $statement = $mysqli->prepare($query);

    if (count($params) > 0) {
        $types = array_reduce($params, function($accumulator, $param) {
            return $accumulator . key($param);
        }, "");

        $values = array_map(function($param) {
            return current($param);
        }, $params);

 
        $statement->bind_param($types, ...$values);
    }

    if ($statement) {
        $statement->execute();
        $result = $statement->get_result();
    
        $response = [''];
    
        if ($mysqli->errno) {
            if (get_config('DEBUG')) {
                set_flash('error', '<h1 style="color: red;">SQL</h1>Query error : ' . $mysqli->error);
            }
    
            return ['success' => false, 'result' => $mysqli->error];
        } 
    
        return ['success' => true, 'result' => $result];
    } else {
        if (get_config('DEBUG')) {
            set_flash('error', '<h1 style="color: red;">SQL</h1>Query error : ' . $mysqli->error);
        }

        return ['success' => false, 'result' => $mysqli->error];
    }
}

function sql_statement($query, $params = []) {
    $response = sql($query, $params);

    if (is_array($response['result']) && get_config('DEBUG')) {
        set_flash('error', '<h1 style="color: red;">PHP</h1>Do not call sql_statement for SQL queries that return rows');
    }

    return $response;
}

function sql_select($query, $params = []) {
    $response = sql($query, $params);

    if (is_bool($response['result']) && get_config('DEBUG')) {
        set_flash('error', '<h1 style="color: red;">PHP</h1>Do not call sql_select for SQL queries that do NOT return rows');
    }

    if ($response['success'] && ! is_bool($response['result'])) {
        $rows = [];
        while($row = $response['result']->fetch_assoc()) {
            array_push($rows, $row);    
            // Need to build the array manually to fetch rows 
            // when using RETURNING with DELETE clause
        }

        $response['result'] = $rows;
    }

    return $response;
}