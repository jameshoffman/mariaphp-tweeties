<?php

$secrets = include('SECRETS.php');

return [
    'DEBUG' => true,
    
    //
    // WARNING
    // Make sure you don't commit/share private database credentials
    //
    'DB_HOST' => $secrets['DB_HOST'],
    'DB_USER' => $secrets['DB_USER'],
    'DB_PASSWORD' => $secrets['DB_PASSWORD'],
    'DB_NAME' => $secrets['DB_NAME'],
    'DB_PORT' => 3306,

    '/' => '/pages/tweeties', 
    // Default file for url /

    'auth' => '/pages/login',
    // Redirect destination when authentication is required
    // applies to pages ending with .auth.php 	
     
    'already_auth' => '/pages/tweeties',  
    // Redirect destination when already logged in, 
    // applies to pages ending with .auth-redirect.php
];
