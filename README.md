# Tweeties

Demo project for [MariaPHP](https://bitbucket.org/jameshoffman/mariaphp/)

- Set database credentials in SECRETS.php

## Dev server

Start local PHP server
```
php -S localhost:8080 webroot/index.php
```

If you want auto-reload with [browser-sync](https://browsersync.io/), also run
```
browser-sync start --proxy "localhost:8080" --files "**/*"
```