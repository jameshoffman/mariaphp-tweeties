<?php

$username = get_param('username');
$password = get_param('password');

$response = sql_select("SELECT * FROM users WHERE username = ? && pwd = SHA2(?, 256);", [
    ['s' => $username], 
    ['s' => $password]
]);

if ($response['success'] && count($response['result']) == 1) {
    set_session('user_id', $response['result'][0]['id']);
    
    redirect('/pages/tweeties');
} else {
    redirect_error('/pages/login', 'Invalid credentials!');
}