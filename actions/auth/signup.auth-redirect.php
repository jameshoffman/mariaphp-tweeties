<?php

$username = get_param('username');
$password = get_param('password');

$response = sql_select("INSERT INTO users VALUES(DEFAULT, ?, SHA2(?, 256)) RETURNING id;", [
    ['s' => $username], 
    ['s' => $password]
]);

if ($response['success']) {
    set_session('user_id', $response['result'][0]['id']);
    redirect('/pages/tweeties');
} else {
    redirect_error('/pages/signup', $response['result']);
}