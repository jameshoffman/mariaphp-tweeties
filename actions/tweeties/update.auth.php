<?php

$id = get_param('id');
$text = get_param('text');
$userId = get_session('user_id');

$response = sql_statement("UPDATE tweeties SET text = ? WHERE id = ? && user_id = ?;", [
    ['s' => $text],
    ['i' => $id],
    ['i' => $userId],
]);

if ($response['success']) {
    redirect_success('/pages/tweetor', "Updated!" );
} else {
    redirect_error('/pages/tweetor', $response['result']);
}
