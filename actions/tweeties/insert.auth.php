<?php

$text = get_param('text');
$userId = get_session('user_id');

$response = sql_statement("INSERT INTO tweeties(text, user_id) VALUES(?, ?);", [
    ['s' => $text],
    ['i' => $userId]
]);

if ($response['success']) {
    redirect('/pages/tweeties');
} else {
    redirect_error('/pages/tweeties', $response['result']);
}
