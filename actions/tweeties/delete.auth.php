<?php

$id = get_param('id');
$userId = get_session('user_id');

$response = sql_select("DELETE FROM tweeties WHERE id = ? && user_id = ? RETURNING text;", [
    ['i' => $id],
    ['i' => $userId]
]);

if ($response['success']) {
    redirect_success('/pages/tweetor', "[ {$response['result'][0]['text']} ] deleted!" );
} else {
    redirect_error('/pages/tweetor', $response['result']);
}
