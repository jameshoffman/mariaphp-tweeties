<?php

$userId = get_session('user_id');
$tweetyId = get_param('tweety');

$redirect = get_param('redirect') ?? '/pages/tweeties';

// Try to delete if already liked
$deleteResponse = sql_select("DELETE FROM likes 
                              WHERE user_id = ? && tweety_id = ?
                              RETURNING true AS `deleted`;", [
                                  ['i' => $userId],
                                  ['i' => $tweetyId],
                              ]);

if ($deleteResponse['success'] && empty($deleteResponse['result'])) {
    // Nothing to delete, must be created

    $insertResponse = sql_statement("INSERT INTO likes VALUES (?, ?);", [
        ['i' => $userId],
        ['i' => $tweetyId],
    ]);

    if ($insertResponse['success']) {
        redirect_success($redirect, "Liked!" );
    } else {
        redirect_error($redirect, $insertResponse['result']);
    }
} else if ($deleteResponse['success']) {
    redirect_success($redirect, "Like removed!" );
}else {
    redirect_error($redirect, $deleteResponse['result']);
}