<?php

$id = get_param('id');
$userId = get_session('user_id');

$tweety = sql_select("SELECT * FROM tweeties WHERE id = ? && user_id = ?;", [
    ['i' => $id],
    ['i' => $userId],
])['result'][0];

?>

<h1 class="inline-block"><a href="/pages/tweeties" style="color: black; text-decoration: none;">Tweeties</a></h1>

<a href="/actions/auth/logout">Log out</a>
<a href="/pages/tweetor" style="margin-left: 16px;">My tweeties</a>

<h2>Edit</h2>

<?php redirect_message() ?>

<?php if ($tweety): ?>
    <form action="/actions/tweeties/update"  style="display: flex; max-width: 100%; margin: 16px 0px;" method="POST">
        <input type="hidden" name="id" value="<?= $tweety['id'] ?>">
        <input style="flex-grow: 1;" placeholder="<?= htmlspecialchars($tweety['text']) ?>" value="<?= htmlspecialchars($tweety['text']) ?>" type="text" name="text">
        <button type="submit">Update</button>
    </form>
<?php else: ?>
    <p>Tweety not found!</p>
<?php endif; ?>


