<h1 class="center">Sign up</h1>

<?php redirect_message() ?>

<form action="/actions/auth/signup" method="POST">
    <input type="text" name="username" placeholder="Username">
    <input type="password" name="password" placeholder="Password">

    <button type="submit">Sign up!</button>
    <a href="/pages/login" class="ml">Cancel</a>
</form>