<?php
    $userId = get_session('user_id');
    $authorId = get_param('user') ?? $userId;

    $authorName = sql_select("SELECT username 
                              FROM users
                              WHERE id = ?
                              LIMIT 1;", [ ['i' => $authorId] ])['result'][0]['username'];

    $response = sql_select("SELECT tweeties.*, COUNT(likes.user_id) AS `likes_count` FROM tweeties 
                            INNER JOIN users ON user_id = users.id
                            LEFT JOIN likes ON tweeties.id = likes.tweety_id
                            WHERE tweeties.user_id = ?
                            GROUP BY tweeties.id
                            ORDER BY created_at DESC;", [ 
                                ['i' => $authorId] 
                            ]);

    $success = $response['success'];
    $tweeties = $response['result'];
?>

<div>
    <h1 class="inline-block"><a href="/pages/tweeties" style="color: black; text-decoration: none;">Tweeties</a></h1>

    <?php if (auth_valid()): ?>
        <a href="/actions/auth/logout">Log out</a>
    <?php else: ?>
        <a href="/pages/login">Log in</a>
    <?php endif; ?>

    <a href="/pages/tweeties" style="margin-left: 16px;">All tweeties</a>

    <?php if($authorName): ?>

        <?php if ($authorId == $userId): ?>
            <h2>Yours</h2>
        <?php else: ?>
                <h2><?= htmlspecialchars($authorName) ?>'s</h2>
        <?php endif; ?>
                    
        <?php redirect_message() ?>

        <?php if (!$success || empty($tweeties)): ?>

            <p>No tweeties yet...</p>

        <?php else: ?>
        <?php   foreach($tweeties as $tweety): ?>

                    <fieldset style="margin: 16px 0px; word-break: break-word;">
                        <legend>
                            <?= $tweety['created_at'] ?>
                        </legend>

                        <div>
                        <?= htmlspecialchars($tweety['text']) ?>
                    </div>

                    <div>
                        <?php if (auth_valid()): ?>
                            <a  
                                href="/actions/tweeties/like?tweety=<?= $tweety['id'] ?>&user=<?= get_session('user_id') ?>&redirect=/pages/tweetor?user=<?= $authorId ?>"
                                style="text-decoration: none;"
                            >
                        <?php endif; ?>

                                <span style="color: red; font-weight: bold;">
                                    <?= $tweety['likes_count'] ?> ❤️
                                </span>
                                
                        <?php if (auth_valid()): ?>
                            </a>
                        <?php endif; ?>
                    </div>

                        <?php if ($authorId == $userId): ?>
                            <div style="margin-top: 8px;">
                                <a href="/pages/tweety?id=<?= $tweety['id'] ?>" style="color: orange;">Edit</a>
                                <a href="/actions/tweeties/delete?id=<?= $tweety['id'] ?>" style="color: red; margin-left: 16px;">Delete</a>
                            </div>
                        <?php endif; ?>
                    </fieldset>

        <?php   endforeach; ?>
        <?php endif; ?>

    <?php else: ?>

        <p>Not a valid user!</p>

    <?php endif; ?>
</div>