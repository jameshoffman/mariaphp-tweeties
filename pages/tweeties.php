<?php
    $search = get_param('search');

    $selectSearch = "%$search%"; 

    $response = sql_select("SELECT tweeties.*, users.username, COUNT(likes.user_id) AS `likes_count` FROM tweeties 
                            INNER JOIN users ON user_id = users.id
                            LEFT JOIN likes ON tweeties.id = likes.tweety_id
                            WHERE text LIKE ?
                            GROUP BY tweeties.id
                            ORDER BY created_at DESC;", [
                              ['s' => $selectSearch]
                          ]);

    $success = $response['success'];
    $tweeties = $response['result'];
?>

<div>
    <h1 class="inline-block"><a href="/pages/tweeties" style="color: black; text-decoration: none;">Tweeties</a></h1>

    <?php if (auth_valid()): ?>
        <a href="/actions/auth/logout">Log out</a>
        <a href="/pages/tweetor" style="margin-left: 16px;">My tweeties</a>
        
        <?php redirect_message() ?>

        <form action="/actions/tweeties/insert"  style="display: flex; max-width: 100%; margin: 16px 0px;" method="POST">
            <input style="flex-grow: 1;" placeholder="What's on your mind?" type="text" name="text">
            <button type="submit">Tweet!</button>
        </form>
    </form>
    <?php else: ?>
        <a href="/pages/login">Log in</a>
    <?php endif; ?>
        
    <?php if (!$success || empty($tweeties) && empty($search)): ?>

        <p>No tweeties yet...</p>

    <?php else: ?>
            
        <details <?= $search ? 'open' : '' ?>>
            <summary>Search</summary>
            
            <!-- Keep as GET so it's easy to share a search url -->
            <form action="/pages/tweeties"  style="display: flex; max-width: 100%; margin-top: 16px;">
                                                                                                                      <!-- ?search="><b>a</b> -->
                <input style="flex-grow: 1;" placeholder="What are you looking for?" type="text" name="search" value="<?= htmlspecialchars($search) ?>">
                <button type="submit">Search</button>
                <a href="/pages/tweeties" style="margin: 6px 0px 0px 16px;">Cancel</a>
            </form>
        </details>
        
        <?php if($search && empty($tweeties)): ?>
            <p>No tweeties matching search criteria!</p>
        <?php else: ?>
            <?php foreach($tweeties as $tweety): ?>
    
                <fieldset style="margin: 16px 0px; word-break: break-word;">
                    <legend>
                        <a href="/pages/tweetor?user=<?= htmlspecialchars($tweety['user_id']) ?>" style="font-weight: bold;"><?= $tweety['username'] ?></a> 
                        <?= $tweety['created_at'] ?>
                    </legend>
    
                    <div>
                        <?= htmlspecialchars($tweety['text']) ?>
                    </div>

                    <div>
                        <?php if (auth_valid()): 
                            $userId = get_session('user_id');
                            $href = "/actions/tweeties/like?tweety={$tweety['id']}";

                            if ($search) {
                                $href .= "&redirect=/pages/tweeties?search=$search";
                            }
                        ?>
                            <a  
                                href="<?= $href ?>"
                                style="text-decoration: none;"
                            >
                        <?php endif; ?>

                                <span style="color: red; font-weight: bold;">
                                    <?= $tweety['likes_count'] ?> ❤️
                                </span>
                                
                        <?php if (auth_valid()): ?>
                            </a>
                        <?php endif; ?>
                    </div>
                </fieldset>
    
            <?php endforeach; ?>
        <?php endif; ?>

    <?php endif; ?>
</div>