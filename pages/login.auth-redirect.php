<h1 class="center">Login</h1>

<?php redirect_message() ?>

<form action="/actions/auth/login" method="POST">
    <input type="text" name="username" placeholder="Username">
    <input type="password" name="password" placeholder="Password">

    <button type="submit">Login</button>
    <a href="/pages/signup" class="ml">Sign up!</a>
</form>

